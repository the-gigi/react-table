import React, { Component } from 'react';
import './App.css';
import Table1 from './components/Table1'
import Table2 from './components/Table2'
import Table3 from './components/Table3'
import Table4 from './components/Table4'
import Table5 from './components/Table5'
import Table6 from './components/Table6'
import Table7 from './components/Table7'
import Table8 from './components/Table8'
import Table9 from './components/Table9'

var data = [
  {id: 1, name: 'Gob', value: '2'},
  {id: 2, name: 'Buster', value: '5'},
  {id: 3, name: 'George Michael', value: '4'}
];


class App extends Component {
  render() {
    return (
      <div className="App">
        {/*<p className="Table-header">Basic Table</p>*/}
        {/*<Table1 data={data}/>*/}
        {/*<p  className="Table-header">Working with Columns</p>*/}
        {/*<Table2 data={data}/>*/}
        {/*<p  className="Table-header">Styling</p>*/}
        {/*<Table3 data={data}/>*/}
        {/*<p  className="Table-header">Selecting Rows</p>*/}
        {/*<Table4 data={data}/>*/}
        {/*<p  className="Table-header">Expanding Rows</p>*/}
        {/*<Table5 data={data}/>*/}
        {/*<p  className="Table-header">Pagination</p>*/}
        {/*<Table6 data={data}/>*/}
        {/*<p  className="Table-header">Adding and Deleting Rows</p>*/}
        {/*<Table7 data={data}/>*/}
        {/*<p  className="Table-header">Cell Editing</p>*/}
        {/*<Table8 data={data}/>*/}
        <p  className="Table-header">Exporting Data</p>
        <Table9 data={data}/>
      </div>
    );
  }
}

export default App;
