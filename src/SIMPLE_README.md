## WebStorm Jest Gotcha

You need to add `--end=jsdom` to jest options in the run configuration. See:

https://intellij-support.jetbrains.com/hc/en-us/community/posts/115000242490-Jest-error-Document-is-undefined