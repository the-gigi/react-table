import React, {Component} from 'react'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table'
import '../css/Table.css'
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css'


class Table8 extends Component {
  render() {
    const cellEditProp = {
      mode: 'click', // 'dbclick' for trigger by double-click
      nonEditableRows: function() {
        return [3];
      }
    }
    return (
      <div>
        <BootstrapTable data={this.props.data}
                        cellEdit={cellEditProp}
        >
          <TableHeaderColumn isKey dataField='id'
          >
            ID
          </TableHeaderColumn>
          <TableHeaderColumn dataField='name'
          >
            Name
          </TableHeaderColumn>
          <TableHeaderColumn dataField='value'
          >
            Value
          </TableHeaderColumn>
        </BootstrapTable>
      </div>
    )
  }
}

export default Table8
